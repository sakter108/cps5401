#include <stdio.h>
#include <math.h>
#include <stdlib.h>
int factorial(int n){
	if (n==0) return 1;
	if (n==1) return 1;
	return n*factorial(n-1);
}


double bessel_function(int n, double x, int m){
	return (pow(-1.0, m)/(factorial(m)*factorial(m+n))) * pow((x/2.0), (2.0*m + n));

}

double alpha(int n, double x, double e){
	double sum = 0;
	for (int m = 0; m < 100000; m++){
		double bessel = bessel_function(n, x, m);

		if (fabs(bessel) < e) break;
		sum += bessel;
		printf("m: %d, bessel: %f, sum: %f\n", m, bessel, sum);
	}
	return sum;
}

int main(){
	int n = 0;
	double x = 0.0;
	double e = 0.0;
	printf("Enter n: ");
	scanf("%d", &n);
	printf("Enter x: ");
	scanf("%lf", &x);
	printf("Enter epsilon: ");
	scanf("%lf", &e);
	
	if (n < 0 || x < 0 || e < 0){
		printf("Plase choose positive values for n, x and epsilon. Exiting...\n");
		exit(0);
	}
        
	if (e>=1){
		printf("Please choose another values of epsilon which is less than 1\n");
		exit(0);
	}

	printf("OUTPUT: %lf\n", alpha(n, x, e));

	return 0;
	}
