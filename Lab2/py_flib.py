#!/usr/bin/python
import math

def func1(x):
  return 3.0 - math.exp(x)


def func2(x):
        return (math.sin(x))**3-math.cos(x)


def func3(x):
  return 3*x*x*x + 2*x*x + x - 15


def func4(x):
	return math.gamma(x + 0.1) - 2
